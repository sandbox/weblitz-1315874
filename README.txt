== Description ==

ytplayer embeds a configurable Youtube player in your Drupal site.

== Installation ==

1. Download, install and enable the module ytplayer
2. (optional) Download, install and enable the module multiblock (if you want to add more than one player)
3. Configure global settings in "Youtube Player" link in "Site Configuration"
4. Configure parameters at block level : you can show also a playlist in your player (if you enable playlist, 
   you have to configure Username and ID of the playlist); if you don't do it, the player will show the video
   corresponding to the video ID you enter. You can also enable jcarousel to skin player on mobile devices.

== Permissions ==

The module defines "modify ytplayer" permission.

== Usage ==

The block is of type "Youtube Player"; after configured and saved, assign it to a region.
If you want to create more than one player, use the "Instances" tab to add others (optional).

== Credits ==

www.weblitz.it 